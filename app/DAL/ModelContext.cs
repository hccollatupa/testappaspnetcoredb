﻿using Microsoft.EntityFrameworkCore;

namespace testappaspnetcore.DAL
{
    public partial class ModelContext : DbContext
    {
        public ModelContext() { }

        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseOracle(@"User Id=OPTIMUS2;Password=Optimus2017SP;Data Source=10.10.35.124:1521/LPSORADS", b => b.UseOracleSQLCompatibility("11"));
            optionsBuilder.LogTo(System.Console.WriteLine);
        }

        public virtual DbSet<Client> Client { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "OPTIMUS2");

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("CLIENT");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(255);

                entity.Property(e => e.AddressInteriorNumber)
                    .HasColumnName("ADDRESS_INTERIOR_NUMBER")
                    .HasMaxLength(10);

                entity.Property(e => e.AddressNumber)
                    .HasColumnName("ADDRESS_NUMBER")
                    .HasMaxLength(6);

                entity.Property(e => e.Block)
                    .HasColumnName("BLOCK")
                    .HasMaxLength(4);

                entity.Property(e => e.Code)
                    .HasColumnName("CODE")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.CorporateName)
                    .HasColumnName("CORPORATE_NAME")
                    .HasMaxLength(120);

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DEPARTMENT_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.DistrictId)
                    .HasColumnName("DISTRICT_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.EconomicActivityId)
                    .HasColumnName("ECONOMIC_ACTIVITY_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(100);

                entity.Property(e => e.FirstSurname)
                    .HasColumnName("FIRST_SURNAME")
                    .HasMaxLength(40);

                entity.Property(e => e.IdentityDocument)
                    .IsRequired()
                    .HasColumnName("IDENTITY_DOCUMENT")
                    .HasMaxLength(15);

                entity.Property(e => e.IdentityDocumentTypeId)
                    .HasColumnName("IDENTITY_DOCUMENT_TYPE_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.IsBillingAutomatic)
                    .HasColumnName("IS_BILLING_AUTOMATIC")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Lot)
                    .HasColumnName("LOT")
                    .HasMaxLength(3);

                entity.Property(e => e.MarriedSurname)
                    .HasColumnName("MARRIED_SURNAME")
                    .HasMaxLength(40);

                entity.Property(e => e.Names)
                    .HasColumnName("NAMES")
                    .HasMaxLength(120);

                entity.Property(e => e.PersonType)
                    .IsRequired()
                    .HasColumnName("PERSON_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.PhantasyName)
                    .HasColumnName("PHANTASY_NAME")
                    .HasMaxLength(120);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(30);

                entity.Property(e => e.SecondSurname)
                    .HasColumnName("SECOND_SURNAME")
                    .HasMaxLength(40);

                entity.Property(e => e.StateId)
                    .HasColumnName("STATE_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusUserId)
                    .HasColumnName("STATUS_USER_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.StreetTypeId)
                    .HasColumnName("STREET_TYPE_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.SunatCiiuId)
                    .HasColumnName("SUNAT_CIIU_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.UrbanizationDescription)
                    .HasColumnName("URBANIZATION_DESCRIPTION")
                    .HasMaxLength(60);

                entity.Property(e => e.UrbanizationTypeId)
                    .HasColumnName("URBANIZATION_TYPE_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.VoidReferenceId)
                    .HasColumnName("VOID_REFERENCE_ID")
                    .HasColumnType("NUMBER(20)");

                entity.Property(e => e.WebSite)
                    .HasColumnName("WEB_SITE")
                    .HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
