﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testappaspnetcore.Models
{
    public class ClientViewModel
    {
        public long Id { get; set; }
        public string CorporateName { get; set; }
    }
}
